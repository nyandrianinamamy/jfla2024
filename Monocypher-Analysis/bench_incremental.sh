# set -e

START_COMMIT="4.0.0"
END_COMMIT="master" # master in May 1 13:31:03 2023 +0200
GITM="git -C ../"
TOTAL_TIME=0
DATE=`date +"%Y-%m-%d %T"`
STAT_FILE="stat-$DATE.csv"
I=0
SAVE_DIR_PREFIX="incremental"


function run() {
    ./frama-c $1
}

function save() {
    analysis_time=($(grep -2 "Execution time per callstack" logs/$LOG_FILE | tail -n 1 | grep -oe '\([0-9.]*\)'))
    analysis_time=${analysis_time[1]}
    alarms=($(grep "alarms generated" logs/$LOG_FILE | grep -oe '\([0-9.]*\)'))
    
    # Read the temp.stat file and extract total_time,max_memory
    total_time=$(grep 'total_time' temp.stat | cut -d'=' -f2)
    max_memory=$(grep 'max_memory' temp.stat | cut -d'=' -f2)

    echo "$COMMIT; $analysis_time; $total_time; $max_memory; $alarms" >> $STAT_FILE
    save_dir=$SAVE_DIR_PREFIX/$COMMIT
    mkdir -p $save_dir
    cp -rf alarms.csv frama.sav conf.yml stat.csv logs/$LOG_FILE $save_dir/
}

echo "commit;analysis_time;total_time;max_memory;alarms" > $STAT_FILE

./clean all
for CURRENT_COMMIT in $($GITM rev-list --reverse $START_COMMIT..$END_COMMIT); do
    ${GITM} checkout $CURRENT_COMMIT
    
    # Run on commit if condition is valid
    # Run only if any *.c or *.h is modified
    if ${GITM} diff-tree --no-commit-id --name-only -r HEAD | grep -q '\.c$\|\.h$'; then
        COMMIT=$(git rev-parse --short $CURRENT_COMMIT)
        LOG_FILE="eva.$I.log"
        # Modify for the intended analysis type
        ./clean build
        ./prepare
        run
        save
        I=$((I+1));
    fi
done