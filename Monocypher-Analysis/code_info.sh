
# Extract LoC, git diff summary and commit message for each commit

START_COMMIT="4.0.0"
END_COMMIT="master"
GITM="git -C ../"

echo "" > info.txt

function extract_diff_summary() {
    local line="$1"
    
    # Extract files changed
    local files=$(echo "$line" | sed -E -n 's/([0-9]+) files changed.*$/\1/p')
    
    # Extract insertions
    local insertions=$(echo "$line" | sed -E -n 's/.* ([0-9]+) insertions\(\+\).*/+\1/p')

    # Extract deletions
    local deletions=$(echo "$line" | sed -E -n 's/.* ([0-9]+) deletions\(-\).*/-\1/p')

    # Concatenate the results
    local result=""
    [[ -n "$files" ]] && result="${files} files"
    [[ -n "$insertions" ]] && result="${result}/${insertions}"
    [[ -n "$deletions" ]] && result="${result}/${deletions}"

    echo "$result"
}

echo "commit;LoC;diff;original_git_diff" > info.txt

# Iterate over all commits
for CURRENT_COMMIT in $($GITM rev-list --reverse $START_COMMIT..$END_COMMIT); do
    $GITM reset --hard
    ${GITM} checkout $CURRENT_COMMIT
    
    # Run on commit if condition is valid
    # Run only if any *.c or *.h is modified
    if ${GITM} diff-tree --no-commit-id --name-only -r HEAD | grep -q '\.c$\|\.h$'; then
        COMMIT=$(git rev-parse --short $CURRENT_COMMIT)
        cloc_brut=$(cloc --exclude-dir=.frama-c --include-lang="C,C/C++ Header" ../*)
        diff_brut=$($GITM --no-pager diff --minimal --stat HEAD^ HEAD)
        unformat_diff_brut=$(echo $diff_brut)
        summary=$(extract_diff_summary "$diff_brut")
        cloc=$(echo "$cloc_brut" | awk '/^SUM:/ { print $NF }')
        echo "$COMMIT;$cloc;$summary;$unformat_diff_brut" >> info.txt
    fi
done