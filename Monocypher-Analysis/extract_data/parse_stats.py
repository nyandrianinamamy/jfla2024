import os
import csv
import argparse
from collections import defaultdict


def process_csv_files(directory, output_file):
    # Dictionary to store the results
    result_dict = defaultdict(lambda: defaultdict(int))
    # Set to store unique values from the first column across all CSV files
    unique_headers = set()

    # Walk through all subdirectories
    for subdir, _, files in os.walk(directory):
        if 'stat.csv' in files:
            subdirectory_name = os.path.basename(subdir)
            csv_path = os.path.join(subdir, 'stat.csv')

            # Open each stat.csv and compute the total value in the fourth column for rows with the same first column value
            with open(csv_path, 'r') as infile:
                for line in infile:
                    row = line.strip().split()
                    if len(row) >= 2:
                        header = row[0]
                        value = int(row[-1])
                        result_dict[subdirectory_name][header] += value
                        unique_headers.add(header)

    # Write the results to result.csv
    with open(output_file, 'w', newline='') as outfile:
        writer = csv.writer(outfile)

        # Write the header row
        writer.writerow(['commit'] + sorted(list(unique_headers)))

        # Write the data rows
        for subdir, values in result_dict.items():
            row = [subdir]
            for header in sorted(list(unique_headers)):
                row.append(values.get(header, 0))
            writer.writerow(row)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Process CSV files in subdirectories.')
    parser.add_argument('directory', type=str,
                        help='The parent directory containing subdirectories with stat.csv files.')
    args = parser.parse_args()

    output_file = f"{args.directory}.stat.csv"
    process_csv_files(args.directory, output_file)

# python3 parse_stats.py self_incremental
