START_COMMIT="4.0.0"
END_COMMIT="master"
GITM="git -C ../"

for CURRENT_COMMIT in $($GITM rev-list --reverse $START_COMMIT..$END_COMMIT); do
    ${GITM} checkout --quiet $CURRENT_COMMIT > /dev/null
    
    # Run on commit if condition is valid
    # Run only if any *.c or *.h is modified
    if ${GITM} diff-tree --no-commit-id --name-only -r HEAD | grep -q '\.c$\|\.h$'; then
        COMMIT=$(git rev-parse --short $CURRENT_COMMIT)
        echo $COMMIT
    fi
done