#!/bin/bash

# Initialize default values
DEFAULT_START_COMMIT="polarssl-1.1.6"
DEFAULT_END_COMMIT="polarssl-1.1.8"
DEFAULT_GITM="git -C ../"
DEFAULT_SAVE_DIR_PREFIX="normal"

# Function to display usage information
function usage() {
  echo "Usage: $0 [options]"
  echo "Options:"
  echo "  -s, --start-commit START_COMMIT   The starting commit hash (default: $DEFAULT_START_COMMIT)"
  echo "  -e, --end-commit END_COMMIT       The ending commit hash (default: $DEFAULT_END_COMMIT)"
  echo "  -g, --git-command GITM            The git command to use (default: $DEFAULT_GITM)"
  echo "  -p, --save-dir-prefix PREFIX      The prefix for save directories (default: $DEFAULT_SAVE_DIR_PREFIX)"
  exit 1
}

# Parse command-line options
while [[ "$#" -gt 0 ]]; do
  case $1 in
    -s|--start-commit) START_COMMIT="$2"; shift ;;
    -e|--end-commit) END_COMMIT="$2"; shift ;;
    -g|--git-command) GITM="$2"; shift ;;
    -p|--save-dir-prefix) SAVE_DIR_PREFIX="$2"; shift ;;
    *) echo "Unknown parameter: $1"; usage ;;
  esac
  shift
done

# Set default values if not provided
START_COMMIT=${START_COMMIT:-$DEFAULT_START_COMMIT}
END_COMMIT=${END_COMMIT:-$DEFAULT_END_COMMIT}
GITM=${GITM:-$DEFAULT_GITM}
SAVE_DIR_PREFIX=${SAVE_DIR_PREFIX:-$DEFAULT_SAVE_DIR_PREFIX}

# Function to run the analysis
function run_analysis() {
    ./frama-c $1
}

# Function to extract diff summary
function extract_diff_summary() {
    local line="$1"
    
    # Extract files changed
    local files=$(echo "$line" | sed -E -n 's/([0-9]+) files changed.*/\1/p')
    
    # Extract insertions
    local insertions=$(echo "$line" | sed -E -n 's/.*([0-9]+) insertions\(\+\).*/+\1/p')

    # Extract deletions
    local deletions=$(echo "$line" | sed -E -n 's/.*([0-9]+) deletions\(-\).*/-\1/p')

    # Concatenate the results
    local result=""
    [[ -n "$files" ]] && result="${files} files"
    [[ -n "$insertions" ]] && result="${result}/${insertions}"
    [[ -n "$deletions" ]] && result="${result}/${deletions}"

    echo "$result"
}

# Function to save the results
function save_results() {
    analysis_time=($(grep -2 "Execution time per callstack" logs/$LOG_FILE | tail -n 1 | grep -oe '\([0-9.]*\)'))
    analysis_time=${analysis_time[1]}
    alarms=($(grep "alarms generated" logs/$LOG_FILE | grep -oe '\([0-9.]*\)'))
    diff=$($GITM --no-pager diff --minimal --stat HEAD^ HEAD)
    msg=$($GITM --no-pager log -n 1 --pretty=format:%s HEAD)
    
    # Read the temp.stat file and extract total_time,max_memory
    total_time=$(grep 'total_time' temp.stat | cut -d'=' -f2)
    max_memory=$(grep 'max_memory' temp.stat | cut -d'=' -f2)

    # Extract modifications summary from diff
    diff_summary=$(extract_diff_summary "$diff")

    echo "$COMMIT; $analysis_time; $total_time; $max_memory; $alarms; $diff_summary; $msg" >> $STAT_FILE
    save_dir=$SAVE_DIR_PREFIX/$COMMIT
    mkdir -p $save_dir
    cp -rf alarms.csv frama.sav conf.yml stat.csv logs/$LOG_FILE $save_dir/
}

# Initialize the stats file
DATE=$(date +"%Y-%m-%d %T")
STAT_FILE="stat-$DATE.csv"
echo "commit;analysis_time;total_time;max_memory;alarms;diff;commit_msg" > $STAT_FILE
I=0

# Main loop to iterate over commits
./clean all
for CURRENT_COMMIT in $($GITM rev-list --reverse $START_COMMIT..$END_COMMIT); do
    $GITM reset --hard
    $GITM checkout $CURRENT_COMMIT
    
    # Run on commit if any *.c or *.h files are modified
    if $GITM diff-tree --no-commit-id --name-only -r HEAD | grep -q '\.c$\|\.h$'; then
        COMMIT=$($GITM rev-parse --short $CURRENT_COMMIT)
        LOG_FILE="eva.$I.log"
        
        # Apply patches and prepare the environment
        $GITM apply .frama-c/polarssl-1.1.7-eva.patch
        ./clean build
        ./prepare
        
        # Run the analysis and save the results
        run_analysis --force
        save_results
        I=$((I + 1))
    fi
done