# set -e

START_COMMIT="polarssl-1.1.6"
END_COMMIT="polarssl-1.1.8"
GITM="git -C ../"
TOTAL_TIME=0
DATE=`date +"%Y-%m-%d %T"`
STAT_FILE="stat-$DATE.csv"
I=0
SAVE_DIR_PREFIX="self_incremental"


function run() {
    ./frama-c $1
}

function extract_diff_summary() {
    local line="$1"
    
    # Extract files changed
    local files=$(echo "$line" | sed -E -n 's/([0-9]+) files changed.*/\1/p')
    
    # Extract insertions
    local insertions=$(echo "$line" | sed -E -n 's/.*([0-9]+) insertions\(\+\).*/+\1/p')

    # Extract deletions
    local deletions=$(echo "$line" | sed -E -n 's/.*([0-9]+) deletions\(-\).*/-\1/p')

    # Concatenate the results
    local result=""
    [[ -n "$files" ]] && result="${files} files"
    [[ -n "$insertions" ]] && result="${result}/${insertions}"
    [[ -n "$deletions" ]] && result="${result}/${deletions}"

    echo "$result"
}

function save() {
    analysis_time=($(grep -2 "Execution time per callstack" logs/$LOG_FILE | tail -n 1 | grep -oe '\([0-9.]*\)'))
    analysis_time=${analysis_time[1]}
    alarms=($(grep "alarms generated" logs/$LOG_FILE | grep -oe '\([0-9.]*\)'))
    diff=$($GITM --no-pager diff --minimal --stat HEAD^ HEAD)
    msg=$($GITM --no-pager log -n 1 --pretty=format:%s HEAD)
    
    # Read the temp.stat file and extract total_time,max_memory
    total_time=$(grep 'total_time' temp.stat | cut -d'=' -f2)
    max_memory=$(grep 'max_memory' temp.stat | cut -d'=' -f2)

    # Extract modifications summary from diff
    diff_summary=$(extract_diff_summary "$diff")

    echo "$COMMIT; $analysis_time; $total_time; $max_memory; $alarms; $diff_summary; $msg" >> $STAT_FILE
    save_dir=$SAVE_DIR_PREFIX/$COMMIT
    mkdir -p $save_dir
    cp -rf alarms.csv frama.sav conf.yml stat.csv logs/$LOG_FILE $save_dir/
}

echo "commit;analysis_time;total_time;max_memory;alarms;diff;commit_msg" > $STAT_FILE

./clean all

for CURRENT_COMMIT in $($GITM rev-list --reverse $START_COMMIT..$END_COMMIT); do
    $GITM reset --hard
    ${GITM} checkout $CURRENT_COMMIT
    
    # Run on commit if condition is valid
    # Run only if any *.c or *.h is modified
    if ${GITM} diff-tree --no-commit-id --name-only -r HEAD | grep -q '\.c$\|\.h$'; then
        COMMIT=$(git rev-parse --short $CURRENT_COMMIT)
        LOG_FILE="eva.$I.log"
        # Modify for the intended analysis type
        $GITM apply .frama-c/polarssl-1.1.7-eva.patch
        ./clean build
        ./prepare
        cp normal/$COMMIT/frama.sav .
        run
        save
        I=$((I+1));
    fi
done