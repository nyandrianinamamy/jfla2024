
# For all folder inside a designated folder, which contains all short commits sha
# Compare A1 A2 alarms, only output new lines from A2

A1="$1"
A2="$2"

# echo $A1

A1_FOLDERS=$(cd "$A1"; ls -d */ | cut -f1 -d'/')
A2_FOLDERS=$(cd "$A2"; ls -d */ | cut -f1 -d'/')

RESULT="stat_alarms.csv"
echo "Commit; Nb new; Alarms" > $RESULT


if [ "${#A1_FOLDERS[@]}" -eq "${#A2_FOLDERS[@]}" ]; then
    for FOLDER in $A1_FOLDERS; do
        DIFF=$(diff --minimal -u <(sort "$A1/$FOLDER/alarms.csv") <(sort "$A2/$FOLDER/alarms.csv") | grep -E "^\+" | tail -n +2)
        count=$(diff --minimal -u <(sort "$A1/$FOLDER/alarms.csv") <(sort "$A2/$FOLDER/alarms.csv") | grep -E "^\+" | tail -n +2 | wc -l)
        unformat=$(echo $DIFF)
        echo "$FOLDER; $count; $unformat" >> $RESULT
    done
else
    echo "Not the same list"
fi
