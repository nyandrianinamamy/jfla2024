import csv
import argparse


def read_and_write_csv(input_file, output_file, column_index):
    with open(input_file, 'r') as infile, open(output_file, 'w', newline='') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)

        # Read and write the header
        header = next(reader)
        writer.writerow([header[0], header[column_index]])

        # Read and write the data rows
        for row in reader:
            writer.writerow([row[0], row[column_index]])


def main():
    parser = argparse.ArgumentParser(
        description="Extract the first and given columns from a CSV file.")
    parser.add_argument("input_file", help="Input CSV file name")
    parser.add_argument("output_file", help="Output CSV file name")
    parser.add_argument("column_index", type=int,
                        help="Column index to extract")

    args = parser.parse_args()

    read_and_write_csv(args.input_file, args.output_file, args.column_index)


if __name__ == "__main__":
    main()

# python extract_columns.py input.csv output.csv 2
