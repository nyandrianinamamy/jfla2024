import os
import re
import csv
import argparse


def extract_numbers(line, pattern):
    match = re.search(pattern, line)
    if match:
        return match.groups()
    return None


def process_log_files(directory, output_file):
    with open(output_file, 'w', newline='') as outfile:
        fieldnames = ['Subdirectory', 'saved_calls', 'saved_functions_calls',
                      'saved_widenings', 'saved_functions_widenings',
                      'reloaded_calls', 'reloaded_functions_calls',
                      'reloaded_widenings', 'reloaded_functions_widenings',
                      'difference_functions', 'percentage_equivalent']
        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
        writer.writeheader()

        for subdir, _, files in os.walk(directory):
            log_files = [f for f in files if re.match(r'eva\.\d+\.log', f)]
            for log_file in log_files:
                log_path = os.path.join(subdir, log_file)
                subdirectory_name = os.path.basename(subdir)
                result = {}
                result['Subdirectory'] = subdirectory_name

                with open(log_path, 'r') as infile:
                    for line in infile:
                        # For lines like "[eva] In save file frama.sav, 46509 saved calls for 159 functions"
                        numbers = extract_numbers(
                            line, r'\[eva\] In save file frama\.sav, (\d+) saved calls for (\d+) functions')
                        if numbers:
                            result['saved_calls'], result['saved_functions_calls'] = numbers

                        # For lines like "[eva] In save file frama.sav, 10 saved widenings for 11 functions"
                        numbers = extract_numbers(
                            line, r'\[eva\] In save file frama\.sav, (\d+) saved widenings for (\d+) functions')
                        if numbers:
                            result['saved_widenings'], result['saved_functions_widenings'] = numbers

                        # For lines like "[eva] In current session, 12505 saved calls for 94 functions"
                        numbers = extract_numbers(
                            line, r'\[eva\] In current session, (\d+) saved calls for (\d+) functions')
                        if numbers:
                            result['reloaded_calls'], result['reloaded_functions_calls'] = numbers

                        # For lines like "[eva] In current session, 10 saved widenings for 11 functions"
                        numbers = extract_numbers(
                            line, r'\[eva\] In current session, (\d+) saved widenings for (\d+) functions')
                        if numbers:
                            result['reloaded_widenings'], result['reloaded_functions_widenings'] = numbers

                    # Compute the difference and percentage
                    if 'saved_functions_calls' in result and 'reloaded_functions_calls' in result:
                        result['difference_functions'] = int(result['saved_functions_calls']) - \
                            int(result['reloaded_functions_calls'])
                        if result['saved_functions_calls'] != 0:
                            result['percentage_equivalent'] = (
                                int(result['reloaded_functions_calls']) / int(result['saved_functions_calls'])) * 100
                        else:
                            result['percentage_equivalent'] = 0.0

                writer.writerow(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Process log files in subdirectories.')
    parser.add_argument('directory', type=str,
                        help='The parent directory containing subdirectories with eva.*.log files.')
    args = parser.parse_args()

    output_file = f"{args.directory}.logs.csv"
    process_log_files(args.directory, output_file)
