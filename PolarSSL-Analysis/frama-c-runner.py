import glob
from io import TextIOWrapper
import os
from pathlib import Path
import time
import yaml
import subprocess as sp
import argparse
from yaml.loader import SafeLoader
from cmdbench import benchmark_command
import json

TIMEOUT = 1000


def run_command(script: str, stdout: TextIOWrapper, timeout=10, figure_image_save_path=None):
    """
    The run_command function takes in a script, output file, cwd (current working directory), and environment variables.
    It then runs the script with the given parameters and returns how long it took to run.

    :param script: str: Pass in the command that will be executed
    :param output: TextIOWrapper: Write the output of the script to a file
    :param cwd: str: Specify the current working directory
    :param env: os._Environ[str]: Pass the environment variables to the subprocess
    :param timeout: Set the timeout for each command
    :return: The time it took to run the command or TIMEOUT
    """

    new_result = benchmark_command(script, iterations_num=1)
    result = new_result.get_first_iteration()
    user_time = result.get("cpu").get("user_time")
    read_chars = result.get("disk").get("read_chars")
    write_chars = result.get("disk").get("write_chars")
    memory = result.get("memory").get("max")

    fig = new_result.get_resources_plot(width=15, height=3)
    fig.savefig(figure_image_save_path)

    stdout.write(result.get("process").get("stdout_data"))
    stdout.write(f"Read chars: {read_chars}\n")
    stdout.write(f"Write chars: {write_chars}\n")
    stdout.write(f"Max Memory: {memory}\n")
    return user_time, memory


def run_analysis(script):
    i = 0
    while os.path.exists(f"logs/eva.{i}.log"):
        i += 1
    out_file = f"logs/eva.{i}.log"

    # Clear the file
    with open(out_file, "w") as out:
        out.write("")

    figure_image_save_path = f"imgs/eva.{i}.png"
    if os.path.exists(figure_image_save_path):
        os.remove(figure_image_save_path)

    with open(out_file, "a") as out:
        user_time, memory = run_command(
            script=script,
            stdout=out,
            timeout=TIMEOUT,
            figure_image_save_path=figure_image_save_path)
        return user_time, memory


def expand_path(search_path: Path, prefix_path: str, pattern: str):
    """
    The expand_path function takes a search_path, prefix_path and pattern as input.
    It then creates a path object from the search_path and prefix path, resolves it to an absolute path
    and returns all files in that directory matching the pattern. The returned list is formatted so that each file name is prefixed with the prefix path.

    :param search_path: Path: Define the path that is used to search for files
    :param prefix_path: str: Specify the prefix path to be prefixed to the final directory
    :param pattern: str: Filter the files in the directory
    :return: A sorted list of files found in the search_path
    """
    p = Path(search_path / prefix_path).resolve()

    lst = list(
        map(lambda f: f"{prefix_path}/{f.name}", list(p.glob(pattern))))
    lst.sort()
    return lst


def get_sources(_sources: list[str]):
    sources = []
    for path in _sources:
        for file in glob.glob(path):
            sources.append(file)
    return " ".join(sources)


def get_value(value: list | str | int | None):

    if value is None:
        return ""
    if isinstance(value, list):
        return ",".join(value)
    if isinstance(value, str) or isinstance(value, int):
        return value

    raise ValueError(f"Value is of unsupported type {type(value)}")


def get_parameters(params, prefix=""):
    params_str = ""

    for item in params:
        if params[item] is None:
            param = f"-{prefix}{item}"
        else:
            param = f"-{prefix}{item}={get_value(params[item])}"
        params_str = params_str + " " + param

    return params_str


def bytes_to_megabytes(bytes_value):
    return bytes_value / 1048576  # 1048576 is 2^20 = 1MB


parser = argparse.ArgumentParser(
    description="Frama C easy runner"
)

parser.add_argument("--force", action="store_true",
                    default=False, dest="force")


if __name__ == "__main__":
    args = parser.parse_args()
    force = args.force

    with open('conf.yml') as f:
        data = yaml.load(f, Loader=SafeLoader)

    if not data:
        raise Exception("Configuration file not found. You don't need me")

    #  FRAMA C
    sources = get_sources(data["frama-c"]["sources"])
    del data["frama-c"]["sources"]
    frama_c_parameters = get_parameters(data["frama-c"])

    # EVA
    # Incremental
    save_file = data["frama-c"]["save"]
    if not force and os.path.exists(save_file):
        data["eva"]["load"] = save_file

    eva_parameters = get_parameters(data["eva"], prefix="eva-")

    # Then
    then = get_parameters(data["then"])

    # script = f"frama-c {frama_c_parameters} {sources} -print -ocode framac.ast -then -no-print"
    script = f"frama-c {frama_c_parameters} {sources} -print -ocode framac.ast -then -no-print -eva {eva_parameters} -then {then}"
    print(f"Analysis command : \n{script}")
    user_time, memory = run_analysis(script)
    memory_mb = bytes_to_megabytes(memory)

    print(
        f"The analysis took: {user_time} seconds, with a max memory of {memory}")
    with open("temp.stat", "w") as out:
        out.write(f"total_time={user_time:.2f}\n")
        out.write(f"max_memory={memory_mb:.2f}\n")
