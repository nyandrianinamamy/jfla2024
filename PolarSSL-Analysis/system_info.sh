# Get CPU information
cpu_info=$(cat /proc/cpuinfo | grep "model name" | uniq)

# Get total memory
total_mem=$(free -h | awk '/^Mem/ {print $2}')

# Get Linux version
linux_version=$(uname -r)

echo "CPU Information: $cpu_info"
echo "Total Memory: $total_mem"
echo "Linux Version: $linux_version"
