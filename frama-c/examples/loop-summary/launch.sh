#!/usr/bin/bash

EVA_FLAGS="-eva-show-perf -eva-precision 3"
SAVE="frama.sav"
INCR=""

if test -f "$SAVE"; then
    rm -rf $SAVE
fi

for FILE in `ls ./source/ | sort -g`; do
    if test -f "$SAVE"; then
        INCR="-eva-load $SAVE"
    fi
    frama-c -save frama.sav source/${FILE} -print -ocode framac.${FILE}.ast -then -no-print -then -eva ${INCR} ${EVA_FLAGS} -eva-statistics-file stat.$FILE.csv > $FILE.log 2>&1
done