#include <stdio.h>
#define N 1
#define BODY for(int m=0; m<10; m++) { \
                for(int n=0; n<10; n++) { \
                    for(int o=0; o<10; o++) { \
                        printf("Nothing\n"); \
                    } \
                } \
            } 

void outer_loop() {
   int i = 0;
    while(i<10) {
        { // Inner loop
            BODY
        }
        i++;
    } 
}

void inner_loop(int *i) {
    BODY
    (*i)++;
}


int f() {
    int f = N;
    { // Outer loop => outer_loop()
        int i = 0;
        while(i<10) {
            { // Inner loop => inner_loop()
                BODY
            }
            i++;
        }
    }
    return f;
}

int h() {
    int h = N;
    {
        int i = 0;
        while(i<10) {
            {
                inner_loop(&i);
            }
        }
    }
    return h;
}

int g() {
    int g = N;
    outer_loop();
    return g;
}


int main() {
    f();
    h();
    g();
    return 0;
}