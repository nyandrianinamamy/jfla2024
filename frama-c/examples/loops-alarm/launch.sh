#!/usr/bin/bash

frama-c -save frama.sav main.c -then -eva -eva-show-perf -eva-precision -1 -eva-flamegraph flame >log 2>&1