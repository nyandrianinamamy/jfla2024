#include <stdio.h>

int global_var = 0;

void increment(int *x)
{
    // Do some analysis here
    if (global_var == 0)
    {
        x[0]++; // this operation will be performed only once
    }
    else
    {
        x[0] += 2;
    }
}

void analyze(int iterations, int *x)
{
    for (int i = 0; i < iterations; i++)
    {
        for (int j = 0; j < iterations; j++)
        {
            for (int k = 0; k < iterations; k++)
            {
                for (int l = 0; l < iterations; l++)
                {
                    for (int m = 0; m < iterations; m++)
                    {
                        increment(x);
                    }
                }
            }
        }
    }
}

int main()
{
    int x[1];
    x[0] = 0;
    int iterations = 10;
    analyze(iterations, x);
    printf("x: %d\n", x[0]);
    return 0;
}
