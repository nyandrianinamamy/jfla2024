#!/usr/bin/bash

EVA_FLAGS="-eva-msg-key=widening,memexec -eva-show-perf -eva-reuse-widenings"
START_COMMIT="fe905e7"
COMMITS="f036e87 6b34908"
GITM="git -C source"
I=0

# Batch
$GITM checkout $START_COMMIT
frama-c -save frama.sav source/main.c -print -ocode framac.ast -then -no-print -then -then -eva ${EVA_FLAGS} -eva-statistics-file stat.$I.$START_COMMIT.csv >log.$I.$START_COMMIT 2>&1

# Incremental
for COMMIT in $COMMITS; do
I=$(($I+1))
$GITM checkout $COMMIT
frama-c -save frama.sav source/main.c -print -ocode framac.ast -then -no-print -then -then -eva -eva-load frama.sav ${EVA_FLAGS} -eva-statistics-file stat.$I.$COMMIT.csv >log.$I.$COMMIT 2>&1
done

# # Incremental + batch (N1)
# frama-c -save frama.sav main.c -cpp-extra-args="-DINCREMENTALN1" -print -ocode framac.incr_1.ast -then -no-print -then -then -eva -eva-load frama.sav ${EVA_FLAGS} -eva-statistics-file stat.incr.1.csv >log.incr.1 2>&1
# frama-c main.c -cpp-extra-args="-DINCREMENTALN1" -print -ocode framac.batch_1.ast -then -no-print -then -then -eva ${EVA_FLAGS} -eva-statistics-file stat.batch.1.csv >log.batch.1 2>&1

# # Incremental + batch (N2)
# frama-c -save frama.sav main.c -cpp-extra-args="-DINCREMENTALN2" -print -ocode framac.incr_2.ast -then -no-print -then -then -eva -eva-load frama.sav ${EVA_FLAGS} -eva-statistics-file stat.incr.2.csv >log.incr.2 2>&1
# frama-c main.c -cpp-extra-args="-DINCREMENTALN2" -print -ocode framac.batch_2.ast -then -no-print -then -then -eva ${EVA_FLAGS} -eva-statistics-file stat.batch.2.csv >log.batch.2 2>&1
