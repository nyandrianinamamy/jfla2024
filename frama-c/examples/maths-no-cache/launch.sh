#!/usr/bin/bash

frama-c -save frama.sav maths.c -then -eva -eva-show-perf -eva-precision 3 -eva-flamegraph flame -eva-no-memexec >log 2>&1