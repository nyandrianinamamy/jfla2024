#!/usr/bin/bash

frama-c -save frama.sav maths.c -then -eva -eva-show-perf -eva-precision 3 -eva-flamegraph flame -eva-load frama.sav >log 2>&1