The following set of packages is known to be a working configuration for
compiling Frama-C+dev.

- OCaml 4.13.1
- alt-ergo-free.2.2.0 (for wp, optional)
- apron.v0.9.13 (for eva, optional)
- dune.3.4.1
- dune-site.3.4.1
- lablgtk3.3.1.2
- lablgtk3-sourceview3.3.1.2
- mlmpfr.4.1.0+bugfix2 (for eva, optional)
- ocamlfind.1.9.5
- ocamlgraph.2.0.0
- ppx_deriving_yojson.3.7.0
- ppx_import.1.10.0
- why3.1.6.0
- yojson.2.0.2
- zarith.1.12
