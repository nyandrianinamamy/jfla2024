;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                        ;;
;;  This file is part of Frama-C.                                         ;;
;;                                                                        ;;
;;  Copyright (C) 2007-2023                                               ;;
;;    CEA (Commissariat à l'énergie atomique et aux énergies              ;;
;;         alternatives)                                                  ;;
;;                                                                        ;;
;;  you can redistribute it and/or modify it under the terms of the GNU   ;;
;;  Lesser General Public License as published by the Free Software       ;;
;;  Foundation, version 2.1.                                              ;;
;;                                                                        ;;
;;  It is distributed in the hope that it will be useful,                 ;;
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of        ;;
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;;
;;  GNU Lesser General Public License for more details.                   ;;
;;                                                                        ;;
;;  See the GNU Lesser General Public License version 2.1                 ;;
;;  for more details (enclosed in the file licenses/LGPLv2.1).            ;;
;;                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(rule
 (alias frama-c-configure)
 (deps (universe))
 (action (progn
          (echo "Callgraph GUI:" %{lib-available:frama-c-callgraph.gui} "\n")
          (echo "  - Frama-C GUI:" %{lib-available:frama-c.gui} "\n")
          (echo "  - Callgraph:" %{lib-available:frama-c-callgraph.core} "\n")
          (echo "  - Ocamlgraph_gtk:" %{lib-available:ocamlgraph_gtk} "\n")
          (echo "  - Ocamlgraph Dgraph:" %{lib-available:ocamlgraph.dgraph} "\n")
  )
  )
)

( library
  (name callgraph_gui)
  (public_name frama-c-callgraph.gui)
  (optional)
  (flags -open Frama_c_kernel -open Frama_c_gui -open Callgraph :standard -w -9)
  (libraries
    frama-c.kernel frama-c.gui frama-c-callgraph.core
    (select graph.ml from
      (!lablgtk3-sourceview3 ocamlgraph.dgraph -> graph.dgraph.ml)
      (!lablgtk3-sourceview3 ocamlgraph_gtk -> graph.gtk.ml)
    )
  )
)

(plugin (optional) (name callgraph-gui) (libraries frama-c-callgraph.gui) (site (frama-c plugins_gui)))
